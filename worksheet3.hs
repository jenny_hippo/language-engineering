--1.1
import Prelude hiding (Just)

data MaybeError a = Error String
                  | Just a
                  deriving (Show)
--fmap :: Functor f => (a -> b) -> (f a) -> (f b)

instance Functor MaybeError where
         fmap g (Error s) = (Error s)
         fmap g (Just a) = (Just (g a))

{-
In order for something to be a functor, it should satisfy two laws.

First law: if we map the id function over a functor, the functor that we get back should be
the same as the original functor

  fmap id (Error s)
= {def fmap}
  Error s

  It holds

  fmap id (Just a)
= {def fmap}
  Just (id a)
= {def id}
  Just a

  It holds

Second law: composing two functions and then mapping the resulting function over a functor
should be the same as first mapping one function over the functor and then mapping the other one

i.e. fmap (f . g) F = fmap f (fmap g F)

  fmap (f . g) Error s
= {def fmap}
  Error s

  fmap f (fmap g (Error s))
= {def fmap}
  fmap f (Error s)
= {def fmap}
  Error s


  Derived to the same expression, therefore it holds

  fmap (f . g) (Just a)
= {def fmap}
  Just (f . g) a
= {def .}
  Just (f (g a))

  fmap f (fmap g (Just a))
= {def fmap}
  fmap f (Just (g a))
= {def fmap}
  Just (f (g a))

  Derived to the same expression, therefore it holds

It holds second law as well, therefore it obeys the Functor laws.
-}

--1.2
data Tree a = Leaf
            | Branch a (Tree a) (Tree a)

instance Functor Tree
  where
    fmap g Leaf = Leaf
    fmap g (Branch a t1 t2) = Branch (g a) (fmap g t1) (fmap g t2)

--1.3
{-
  fmap id Leaf
= {def fmap}
  Leaf

  fmap id (Branch a t1 t2)
= {def fmap}
  Branch (id a) (fmap id t1) (fmap id t2)
= {using fmap id x = x}
  Branch (id a) (t1) (t2)
= {def id}
  Branch (a t1 t2)

First law holds.

  fmap (f . g) Leaf
= {def fmap}
  Leaf

  fmap f (fmap g (Leaf))
= {def fmap}
  fmap f Leaf
= {def fmap}
  Leaf

  fmap (f . g) (Branch a t1 t2)
= {def fmap}
  Branch ((f . g) a) (fmap (f . g) t1) (fmap (f . g) t2)
= {def (.)}
  Branch (f (g a)) (fmap f (fmap g t1)) (fmap f (fmap g t2))

  fmap f (fmap g (Branch a t1 t2))
= {def fmap}
  fmap f (Branch (g a) (fmap g t1) (fmap g t2))
= {def fmap}
  Branch (f (g a)) (fmap f (fmap g t1)) (fmap f(fmap g t2))

which indicates second law also holds.

-}

--1.4
-- * -> *

--1.5
data Exception e a = Except e | Result a
-- * -> * -> *

--1.6
-- * -> *

--1.7
-- it is * -> * -> * where a functor should have the kind * -> *
instance Functor (Exception e)
  where
      fmap g (Except e) = (Except e)
      fmap g (Result a) = (Result (g a))

--2.1
data TreeF a k = LeafF | BranchF a k k
data Fix f = In (f (Fix f))

--2.2

instance Functor (TreeF a)
  where
    fmap g LeafF = LeafF
    fmap g (BranchF l t1 t2) = BranchF l (g t1) (g t2)

--2.3
sumTree :: Tree Int -> Int
sumTree Leaf = 0
sumTree (Branch a t1 t2) = a + sumTree t1 + sumTree t2

sumTreeF :: Fix (TreeF Int) -> Int
sumTreeF = cata alg
  where alg :: TreeF Int Int -> Int
        alg LeafF = 0
        alg (BranchF a t1 t2) = a + t1 + t2

cata :: Functor f => (f b -> b) -> Fix f -> b
cata alg (In x) = alg . fmap (cata alg) $ x

--2.4
countLeaf :: Tree Int -> Int
countLeaf Leaf = 1
countLeaf (Branch a t1 t2) = countLeaf t1 + countLeaf t2

countLeafF :: Fix (TreeF Int) -> Int
countLeafF = cata alg
  where alg :: TreeF Int Int -> Int
        alg LeafF = 1
        alg (BranchF a t1 t2) = t1 + t2


testTree :: Tree Int -- it is predefined as Tree Integer
testTree = Branch 3 (Branch 2 Leaf Leaf) Leaf
testTreeF :: Fix (TreeF Int)
testTreeF = In (BranchF 2 (In (BranchF 3 (In LeafF) (In LeafF)))
                          (In (BranchF 4 (In LeafF) (In LeafF))))
{-
treeAlg :: TreeF a a -> Tree a
treeAlg (LeafF) = Leaf
treeAlg (BranchF a t1 t2) = Branch a t1 t2
-}
