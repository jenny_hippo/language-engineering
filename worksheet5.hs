import Yoda

--type Parser = Parsec () String

{-
Here is a grammar we might want to parse:

<expr> ::= <term> ("+" <term>)*
<term> ::= ('0' | '1' | ... | '9')+

-}

data Expr = ExprTerm Term [Term] deriving Show
data Term = Term Int deriving Show

expr :: Parser Expr
expr = ExprTerm <$> term <*> many (string "+" *> term)

decimal :: Parser Int
decimal = read <$> some (oneOf "0123456789")

term :: Parser Term
term = Term <$> decimal

x = some (oneOf "0123456789") -- Parser [Char]
y = oneOf "0123456789" -- Parser Char
{- We test this by executing the following in ghci:

< parseMaybe expr "1+2+3"

-}

-- 1.1.1
--run with parseMaybe f "the string"
-- a) string :: String -> Parser String
stringParse :: Parser String
stringParse = string "Hello"

-- b) many :: f a -> f [a] (it outputs Just [] if doens't match)
manyParse :: Parser [String]
manyParse = many stringParse

-- c) some :: f a -> f [a] (it outputs 'Nothing' if doesn't match)
someParse :: Parser [String]
someParse = some stringParse

-- d) oneOf :: [Char] -> Parser Char
oneOfParse :: Parser [Char]
oneOfParse = many (oneOf "Hello")

-- e) noneOf :: [Char] -> Parser Char
noneOfParse :: Parser [Char]
noneOfParse = many (noneOf "Hello")

-- 1.1.2
whitespace :: Parser ()
whitespace = () <$ many (oneOf " \t")
-- use parse instead so that it produces tuple (String, a)

-- 1.1.3
tok :: String -> Parser String
tok xs = whitespace *> string xs <* whitespace

-- 1.1.4
number :: Parser Int
number = whitespace *> decimal <* whitespace

-- 1.2.1
data Robot = Stop
             | Fw Int Robot
             | L Robot
             | R Robot
             deriving Show

parseRobot :: Parser Robot
parseRobot = Stop <$ tok "stop" <|>
             L <$ tok "rotate left" <*> parseRobot <|>
             R <$ tok "rotate right" <*> parseRobot <|>
             Fw <$ tok "forward" <*> number <*> parseRobot

-- 1.2.2
punc :: Parser ()
punc = () <$ many (oneOf " ,;\t\n")

tok' :: String -> Parser String
tok' xs = punc *> string xs <* punc
-- replace tok w/ tok'

-- 1.3.1
-- It is using left recursion
-- 1.3.2
data DataB = A Int DataB'| D DataB' deriving Show
data DataB' = B Int DataB' | E deriving Show

parseBad :: Parser DataB
parseBad = A <$ tok "a" <*> number <*> parseBad'
       <|> D <$> parseBad'

parseBad' :: Parser DataB'
parseBad' = B <$ tok "a" <*> number <*> parseBad'
       <|> pure E
