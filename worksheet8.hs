import Yoda
import Control.Applicative

-- 1.2.1
{-
left identity:  return a >>= f  = f a
right identity: m >>= return    = m
associativity:  (m >>= f) >>= g = m >>= (\x -> f x >>= g)
-}

-- 1.2.2
-- data Maybe a = Just a | Nothing
--
-- instance Monad Maybe where
--   return x = Just x
--   (Just x) >>= f = f x
--   (Nothing) >>= f = Nothing

{-
for Just x

  return x >>= f = f x
  return x >>= f
= {return x = Just x}
  Just x >>= f
= {def >>=}
  f x         -- left identity

  m x >>= return = m x
  m x >>= return
= {let m x = Just x}
  Just x >>= return
= {def >>=}
  return x
= {return x = Just x}
  Just x
= {assumption}
  m x         -- right identity


  (Nothing) >>= f = Nothing
= {let f = return}
  (Nothing) >>= return = Nothing  -- right identity

  (mx >>= f) >> = g           = mx >>= (\x -> f x >>= g)
= {let mx = Leaf x}           = {let mx = Leaf x}
  (Leaf x >>= f) >>= g          Leaf x >>= (\x -> f x >>= g)
= {def >>=}                   = {def >>=}
  f x >>= g                     (\x -> f x >>= g) x
                              = {application}
                                f x >> = g
-- associativity

for Nothing?

-}
-- 1.2.3 (not sure)
-- instance Functor (Monad M) where
--   fmap f (return x) = return (f x)
--   fmap f (>>=) x = f x

-- 1.2.4
data Exception e a = Throw e | Continue a

instance Functor (Exception e) where
  fmap f (Throw e) = Throw e
  fmap f (Continue a) = Continue (f a)

-- 1.2.5 (not sure)
instance Applicative (Exception e) where
  pure = Continue

-- <*> :: Exception e (a -> b) -> Exception e a -> Exception e b
  Throw e <*> es = Throw e
  Continue a <*> es = fmap a es

instance Monad (Exception e) where
  return = pure

-- (>>=) :: Exception e a -> (a -> Expection e b) -> Exception e b
  (Throw e) >>= f = Throw e
  (Continue a) >>= f = f a

-- 1.3.1
data Tree a = Leaf a
            | Fork (Tree a)(Tree a)
            deriving Show

instance Functor Tree where
  fmap f (Leaf x) = Leaf (f x)
  fmap f (Fork l r)  = Fork (fmap f l)(fmap f r)

instance Applicative Tree where
-- pure :: a -> Tree a
   pure = Leaf

-- <*> :: Tree (a -> b) -> Tree a -> Tree b
   Leaf f <*> tz = fmap f tz
   Fork l r <*> tz = Fork (l <*> tz)(r <*> tz)

instance Monad Tree where
--return :: a -> Tree a
  return = Leaf
--(>>=) :: Tree a -> (a -> Tree b) -> Tree b
  Leaf x >>= f = f x
  Fork l r >>= f = Fork (l >>= f)(r >>= f)

-- 1.3.2
{- for Leaf x

  return x >>= f = f x
  return x >>= f
= {return x = Leaf x}
  Leaf x >>= f
= {def >>=}
  f x         -- first rule check

  m x >>= return = m x
  m x >>= return
= {let m x = Leaf x}
  Leaf x >>= return
= {def >>=}
  return x
= {return x = Leaf x}
  Leaf x
= {assumption}
  m x            -- second rule check

  (mx >>= f) >> = g           = mx >>= (\x -> f x >>= g)
= {let mx = Just x}           = {let mx = Leaf x}
  (Leaf x >>= f) >>= g          Leaf x >>= (\x -> f x >>= g)
= {def >>=}                   = {def >>=}
  f x >>= g                     (\x -> f x >>= g) x
                              = {application}
                                f x >> = g
-- third rule check
-}

-- 1.3.3
intToDoubleTree :: Int -> Tree Int
intToDoubleTree x = pure (x + x)

-- 1.3.4
addToTree :: Int -> Int -> Tree Int
addToTree x y = pure (x + y)

-- 1.3.5
manipTree :: Tree Int -> Tree Int
--(>>=) :: Tree a -> (a -> Tree b) -> Tree b
manipTree t = t >>= (addToTree 3) >>= intToDoubleTree >>= (addToTree 2)

-- 1.3.6
dupToTree :: a -> Tree a
dupToTree x = Fork (Leaf x)(Leaf x)

-- 1.3.7
dupWholeTree:: Tree a -> Tree a
dupWholeTree t = t >>= dupToTree

-- 1.3.8
doubleWholeTree :: Tree Int -> Tree Int
doubleWholeTree t = dupWholeTree t >>= intToDoubleTree
