import Control.Applicative

-- 1.1.1
data Maybe' a = Nothing' | Just' a

instance Functor Maybe' where
  fmap _ Nothing' = Nothing'
  fmap f (Just' a)  = Just' (f a)

instance Applicative Maybe' where
-- pure :: a -> Maybe' a
   pure = Just'

-- <*> :: Maybe' (a -> b) -> Maybe' a -> Maybe' b
   Nothing' <*> _ = Nothing'
   _ <*> Nothing' = Nothing'
   Just' pf <*> Just' px = Just' (pf px)

-- 1.1.2
-- a) pure id <*> v = v
{-
pure id <*> v
{def pure}
= Just' id <*> v
if (v == Just' a)
= Just' id <*> Just' a
{def <*>}
= Just' (id a)
{def id}
= Just' a
elseif (v == Nothing')
= Just' id <*> Nothing'
{def <*>}
= Nothing'


to be continued
-}
-- 1.2
data List a = Empty | Cons a (List a) deriving Show

-- instance Monoid (List a) where
--     mempty = Empty
--     Empty `mappend` y = y
--     <> :: List a -> List a -> List a
--     Cons x xs `mappend` ys = Cons x (xs <> ys)

-- 1.2.1
instance Functor List where
--  fmap :: (a -> b) -> List a -> List b
    fmap _ Empty = Empty
    fmap f (Cons x xs) = Cons (f x) (fmap f xs)

-- 1.2.2
(<$$) :: a -> List b -> List a
(<$$) = fmap . const

-- 1.2.3
instance Applicative List where
--  pure :: a -> List a
    pure a = Cons a Empty

--  <*> :: List (a -> b) -> List a -> List b
    Empty <*> _ = Empty
    _ <*> Empty = Empty
    (Cons f fs) <*> as = newConc (fmap f as) (fs <*> as)

newConc :: List a -> List a -> List a
newConc Empty ys = ys
newConc ys Empty = ys
newConc (Cons x xs) ys = Cons x (newConc xs ys)
-- 1.2.4
(<**) :: List a -> List b -> List a
px <** py = const <$> px <*> py

-- 1.2.5
(**>) :: List a -> List b -> List b
px **> py = flip const <$> px <*> py
-- px **> py = const <$> py <*> px

-- 1.2.6
instance Alternative List where
--  empty :: List a
    empty = Empty

--  (<|>) :: List a -> List a -> List a
    Empty <|> ys = ys
    xs <|> _ = xs

-- 1.2.7
{-
a) Cons :: a -> List a -> List a
b) Cons <$> :: Functor f => f a -> f (List a -> List a)
c) <$ :: Functor f => a -> f b -> f a
d) Cons <$ :: Functor f => f b -> f (a -> List a -> List a)
e) Cons <$> Just 3 <*> Just (Cons 2 Empty) :: Num a => Maybe (List a)
f) Cons <$> Just 3 :: Num a => Maybe (List a -> List a)
g) Cons <$> (Cons 5 (Empty)) :: Num a => List (List a -> List a)
h) Cons 3 <$> :: Num a => f (List a) -> f (List a)
-}

-- 1.2.8
{-
a) Cons <$> Just 3 <*> Just (Cons 2 Empty) = Just (Cons 3 (Cons 2 Empty)
b) Cons <$> Just 3 <* Just (Cons 2 Empty) <*> Just (Cons 1 Empty) = Just (Cons 3 (Cons 1 Empty))
c) Cons <$> Just 3 *> Just 2 <* Just (Cons 1 Empty) = Just 2      [why does it work?]
d) Just <$> Cons 5 (Cons 2 Empty) = Cons (Just 5) (Cons (Just 2) Empty)
e) Just <$> Cons 5 (Cons 2 Empty) *> Cons 3 (Cons 4 Empty) = Cons 3 (Cons 4 (Cons 3 (Cons 4 Empty)))
f) Just <$> Cons 5 (Cons 2 Empty) <* Cons 3 (Cons 4 Empty) = Cons (Just 5) (Cons (Just 5) (Cons (Just 2) (Cons (Just 2) Empty)))
-}

-- 1.2.9
liftATwo :: Applicative f => (a -> b -> c) -> f a -> f b -> f c
liftATwo f x y = f <$> x <*> y
