{-# LANGUAGE RankNTypes #-}
-- 1.1.1
data Fix f = In ( f (Fix f))

-- 1.1.2
cata :: Functor f => (f b -> b) -> Fix f -> b
cata alg (In x) = alg . fmap (cata alg) $ x

-- 1.2.1
data Robot' k = Stop
             | Fw Int k
             | L k
             | R k

-- 1.2.2
instance Functor Robot'
   where
     fmap g Stop = Stop
     fmap g (L k) = L (g k)
     fmap g (R k) = R (g k)
     fmap g (Fw a k) = Fw a (g k)

-- 1.2.3
{-
fmap id Stop
= {def fmap}
Stop

fmap id (L k)
= {def fmap}
L (id k)
= {def id}
L k

fmap id (R k)
= {def fmap}
R (id k)
= {def id}
R k

fmap id (Fw a k)
= {def fmap}
Fw a (id k)
= {def id}
Fw a k

------First law holds------

fmap (f . g) Stop
= {def fmap}
Leaf

fmap f (fmap g (Stop))
= {def fmap}
fmap f Stop
= {def fmap}
Stop


fmap (f . g) (L k)
= {def fmap}
L ((f . g) k)
= {def .}
L (f (g k))

fmap f (fmap g (L k))
= {def fmap}
fmap f (L (g k))
= {def fmap}
L (f (g k))


fmap (f . g) (R k)
= {def fmap}
R ((f . g) k)
= {def .}
R (f (g k))

fmap f (fmap g (R k))
= {def fmap}
fmap f (R (g k))
= {def fmap}
R (f (g k))


fmap (f . g) (Fw a k)
= {def fmap}
Fw a ((f . g) k)
= {def .}
Fw a (f (g k))

fmap f (fmap g (Fw a k))
= {def fmap}
fmap f (Fw a (g k))
= {def fmap}
Fw a (f (g k))
-}

-- 1.2.4
disTrav :: Fix Robot' -> Int
disTrav = cata alg
    where
      alg :: Robot' Int -> Int
      alg (Stop)    = 0
      alg (L k)     = k
      alg (R k)     = k
      alg (Fw a k)  = a + k

testRobot = In ( Fw 10 ( In ( R ( In ( Fw 20 ( In ( L ( In ( Fw 10 ( In ( L ( In ( Fw 10 ( In ( R ( In ( Stop )))))))))))))))))
testRobot' = In ( Fw 10 ( In ( R ( In ( Fw 20 ( In ( L ( In ( Fw 10 ( In ( L ( In ( Fw 10 ( In ( Stop )))))))))))))))
testRobot'' = In ( R ( In ( Fw 10 ( In ( R ( In ( Fw 20 ( In ( L ( In ( Fw 10 ( In ( L ( In ( Fw 10 ( In ( Stop )))))))))))))))))
testRobot''' = In ( R ( In ( R ( In ( Fw 10 ( In ( R ( In ( Fw 20 ( In ( L ( In ( Fw 10 ( In ( L ( In ( Fw 10 ( In ( Stop )))))))))))))))))))

-- 1.2.5  -- Charlie's solution using transformation of the map itself instead of person turning
distxy :: Fix Robot' -> (Int, Int)
distxy = cata alg
    where
      alg :: Robot' (Int, Int) -> (Int, Int) -- (distance, direction)
      alg (Stop)   = (0, 0)
      alg (L (x, y))    = (-y, x)
      alg (R (x, y))    = (y, -x)
      alg (Fw a (x, y)) = (a + x, y)

distx :: Fix Robot' -> Int
distx = fst . distxy

-- 1.2.6
disty :: Fix Robot' -> Int
disty = snd . distxy

disp :: Fix Robot' -> Float
disp r = sqrt (fromIntegral((distx r) ^ 2) + fromIntegral((disty r) ^ 2))

-- 1.2.7
{-
func = h . (cata a)
func = h . a . (fmap (cata a)) . in0
func = b . (fmap h) . (fmap (cata a)) . in0
func = cata b
-}

-- 1.3.1
mcata :: (forall x. (x -> a) -> (f x -> a)) -> Fix f -> a
mcata phi (In x) = phi (mcata phi) x

-- 1.3.2
data List a x = Empty
              | Cons a x

len_alg :: (x -> Int) -> (List b x -> Int)
len_alg g Empty      = 0
len_alg g (Cons b k) = 1 + (g k)

-- 1.3.3
sum_alg :: (x -> Int) -> (List Int x -> Int)
sum_alg g Empty      = 0
sum_alg g (Cons b k) = b + (g k)

-- 1.3.4
mLen_alg :: Fix (List a) -> Int
mLen_alg = mcata len_alg

mSum_alg :: Fix (List Int) -> Int
mSum_alg = mcata sum_alg

-- 1.3.5
