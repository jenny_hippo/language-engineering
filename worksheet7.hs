import Text.Megaparsec
-- import Megaparsec.String
import Control.Applicative
import Data.Monoid

data Chess = Turn Move Move Chess
           | EndGame
             deriving Show

data Move = Move MoveP Quant
          | Cstl Bool
          | End Winner
            deriving Show

data Quant = Prom Piece Quant
           | Chck Quant
           | Null
             deriving Show

data MoveP = Alg Piece Cell
           | Smh Cell Cell
           | AlgDis Piece Cell Cell
           | Tke Piece Cell
             deriving Show

data Winner = White
            | Black
            | Draw
            | AO
              deriving Show

data Cell = Cell Char Int
            deriving (Show, Eq)

data Piece = King
           | Queen
           | Knight
           | Rook
           | Bishop
           | Pawn
             deriving (Show, Eq)
