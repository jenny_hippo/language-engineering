-- 1.1.2
--Q2
data Robot = Stop
             | Fw Int Robot
             | L Robot
             | R Robot

disTrav :: Robot -> Int
disTrav (Stop) = 0
disTrav (Fw i r) = i + disTrav r
disTrav (L r) = disTrav r
disTrav (R r) = disTrav r

--Q3
data Direction = N | E | S | W
turn :: Robot -> Direction -> Direction
turn (L r) N = W
turn (L r) E = N
turn (L r) W = S
turn (L r) S = E
turn (R r) N = E
turn (R r) E = S
turn (R r) S = W
turn (R r) W = N

dD :: Robot -> Int
dD r = dStr8 N r
dStr8 :: Direction -> Robot -> Int
dStr8 _ (Stop) = 0
dStr8 d (L r) = dStr8 (turn (L r) d) r
dStr8 d (R r) = dStr8 (turn (R r) d) r
dStr8 N (Fw i r) = i + dStr8 N r
dStr8 d (Fw i r) = dStr8 d r

{-}
deepDisp :: Direction -> Int -> Int -> Robot -> Float
deepDisp _ x y (Stop) = sqrt (fromIntegral(x*x) + fromIntegral(y*y))
deepDisp d x y (L r) = deepDisp (turn (L r) d) r
deepDisp d x y (R r) = deepDisp (turn (R r) d) r
disDir :: Int -> Robot -> Int
--        dis   angle
disDir _ (Stop) = 0
disDir a (L r) = disDir (a-90) (r)
disDir a (R r) = disDir (a+90) (r)
disDir a (Fw i r)
    | a `mod` 360 == 0  = i + disDir a r
    | otherwise         = disDir a r
-}
--Q4
disp :: Int -> Int -> Int -> Robot -> Float
disp x y _ (Stop) = sqrt (fromIntegral(x*x) + fromIntegral(y*y))
disp x y a (L r) = disp x y (a - 90) r
disp x y a (R r) = disp x y (a + 90) r
disp x y a (Fw i r)
    | a `mod` 360 == 0  = disp (x + i) y a r
    | a `mod` 360 == 90  = disp x (y + i) a r
    | a `mod` 360 == 180  = disp (x - i) y a r
    | a `mod` 360 == 270  = disp x (y - i) a r
--}
test = Fw 10 (R ( Fw 10 (R ( Fw 20 (R (Fw 20 (R (Fw 30 (R (Fw 30 (Stop)))))))))))
testRobot = Fw 20 ( R ( Fw 20 ( L ( Fw 10 ( L ( Fw 10 ( R ( Stop ))))))))

--1.2.2
--Q1
type Veg = (Int, Int, Bool, String)

potato :: (Int, Int, Bool, String)
potato = (0 ,3, False, "potato")
--Q2
peel :: (Int, Int, Bool, String) -> (Int, Int, Bool, String)
peel (t, w, c, d) = ((t+2), w, c, d ++ " peeled")

roast :: (Int, Int, Bool, String) -> (Int, Int, Bool, String)
roast (t, w, c, d) = ((t+70), w, True, d ++ " roasted")

boil :: (Int, Int, Bool, String) -> (Int, Int, Bool, String)
boil (t, w, c, d) = ((t+25), w, True, d ++ " boiled")

mash :: (Int, Int, Bool, String) -> (Int, Int, Bool, String)
mash (t, w, c, d) = ((t+1), w, c, d ++ " mashed")

stew :: (Int, Int, Bool, String) -> (Int, Int, Bool, String)
stew (t, w, c, d) = ((t+120), w, True, d ++ " stewed")
--Q3
mix :: (Int, Int, Bool, String) -> (Int, Int, Bool, String) -> (Int, Int, Bool, String)
mix (t1, w1, c1, d1) (t2, w2, c2, d2) = ((t1+t2), (w1+w2), c1&&c2, d1 ++ "; " ++ d2)
--Q4
carrot :: (Int, Int, Bool, String)
carrot = (0 ,1, False, "carrot")
taro :: (Int, Int, Bool, String)
taro = (0, 5, False, "taro")

--1.3
--Q1
{-GPL is Turing-complete, meaning they can compute any function
and is broadly applicable across application domains. On the other hand, DSL is
specialised for specific uses and may not be Turing-complete. GPL can be split
into imperative languages, where each computational step is clearly stated, and
declarative. Declarative then can have either a functional paradigm (e.g. Haskell),
meaning it is a pure language that compose functions together for computation,
or a logic paradigm.
Q2
-}
