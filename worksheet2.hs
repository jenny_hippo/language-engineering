--1.1
import Data.List
data Animal = Lion | Tiger | Gazelle | Ant
              deriving (Eq, Show)

--1.2.1
type Plot = [[Animal]]
test2 = quadrant (territory [Lion, Lion, Gazelle]) (territory [Tiger]) (territory [Ant, Lion])
       (tridrant (territory []) (territory [Tiger, Tiger, Gazelle, Ant, Ant]) (territory []))
       
territory :: [Animal] -> Plot
territory as = [as]

quadrant :: Plot -> Plot -> Plot -> Plot -> Plot
quadrant p1 p2 p3 p4 = p1 ++ p2 ++ p3 ++ p4

tridrant ::  Plot -> Plot -> Plot -> Plot  --1.2.3
tridrant p1 p2 p3 = p1 ++ p2 ++ p3

numTers :: Plot -> Int
numTers p = length p

mostAnimals :: Plot -> Int
mostAnimals = maximum . (map length)
--1.2.4
listAnimals :: Plot -> [Animal]
listAnimals = nub . concat

--1.2.2 deep embedding
data PlotD = Territory [Animal]
            | Quadrant PlotD PlotD PlotD PlotD
            | Tridrant PlotD PlotD PlotD --1.2.3
test3 = Quadrant (Territory [Lion, Lion, Gazelle]) (Territory [Tiger]) (Territory [Ant, Lion])
       (Tridrant (Territory []) (Territory [Tiger, Tiger, Gazelle, Ant, Ant]) (Territory []))
test4 = Quadrant (Territory [Lion, Lion, Gazelle]) (Territory [Tiger]) (Territory [Ant, Lion])
       (Quadrant (Territory []) (Territory [Tiger, Tiger, Gazelle, Ant, Ant]) (Territory [Ant]) (Territory []))

numTersD :: PlotD -> Int
numTerD (Territory as) = 1
numTersD (Quadrant p1 p2 p3 p4) = (sum . map numTersD) [p1, p2, p3, p4]
numTersD (Tridrant p1 p2 p3) = (sum . map numTersD) [p1, p2, p3]

mostAnimalsD :: PlotD -> Int
mostAnimalsD (Territory as) = length as
mostAnimalsD (Quadrant p1 p2 p3 p4) = maximum $ (map mostAnimalsD) [p1, p2, p3, p4]
mostAnimalsD (Tridrant p1 p2 p3) = maximum $ (map mostAnimalsD) [p1, p2, p3]

--1.2.4
listAnimalsD :: PlotD -> [Animal]
listAnimalsD (Territory as) = nub as
listAnimalsD (Quadrant p1 p2 p3 p4) = nub $ concat $ (map listAnimalsD) [p1, p2, p3, p4]
listAnimalsD (Tridrant p1 p2 p3) = nub $ concat $ (map listAnimalsD) [p1, p2, p3]

--1.2.5
{-
Uses more memory because each character in the string has to be stored,
whereas the storage needs for a data type isn't dependent on the length
of the constructor names.
Prevent users from adding info that's not included.
-}

--1.2.6
data Ploot = Territory' [Animal]
           | Split [Ploot]

test' = Split [(Territory' [Lion, Lion, Gazelle]), (Territory' [Tiger]), (Territory' [Ant, Lion]),
        Split [(Territory' []), (Territory' [Tiger, Tiger, Gazelle, Ant, Ant]), (Territory' [Ant]), (Territory' [])]]

numTersD' :: Ploot -> Int
numTersD' (Territory' xs) = 1
numTersD' (Split [])      = 0
numTersD' (Split (p:ps))  = numTersD' p + numTersD' (Split ps)

mostAnimalsD' :: Ploot -> Int
mostAnimalsD' (Territory' xs) = length xs
mostAnimalsD' (Split [])      = 0
mostAnimalsD' (Split (p:ps))  = maximum $ (map mostAnimalsD') ps

listAnimalsD' :: Ploot -> [Animal]
listAnimalsD' (Territory' xs) = nub xs
listAnimalsD' (Split [])      = []
listAnimalsD' (Split ps)  = nub $ concat $ (map listAnimalsD') ps

--1.2.7
toPloot :: PlotD -> Ploot
toPloot (Territory xs) = Territory' xs
toPloot (Quadrant p1 p2 p3 p4)  = Split ((map toPloot) [p1, p2, p3, p4])
toPloot (Tridrant p1 p2 p3)     = Split ((map toPloot) [p1, p2, p3])

--1.2.8
quad :: Ploot -> Ploot -> Ploot -> Ploot -> Ploot
quad p1 p2 p3 p4 = Split [p1, p2, p3, p4]

--1.2.9
trid :: Ploot -> Ploot -> Ploot -> Ploot
trid p1 p2 p3 = Split [p1, p2, p3]

--1.3.1
{-
Advantages of shallow over deep embedding:
    1. Shallow embedding use less code does not need base case.
    2. Uses existing data types (can mean less code)

  1.3.2

  Advantages of deep over shallow embedding
    1. Clearer code
    2. Allows pattern matching
    3. Add new constructors easily
    4. Easy to add new functionality without having to change all previous functions

  1.3.3

  A deep core language allows for all the clarity and flexibilty of deep embedding
  but smart constructors allow for the language to be restricted for users, so
  the system doesn't break.
-}
